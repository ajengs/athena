class AddRoleToMenus < ActiveRecord::Migration[5.2]
  def change
    add_reference :menus, :role, foreign_key: true
  end
end
