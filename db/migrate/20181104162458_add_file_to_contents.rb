class AddFileToContents < ActiveRecord::Migration[5.2]
  def change
    add_column :contents, :attachment, :text
  end
end
