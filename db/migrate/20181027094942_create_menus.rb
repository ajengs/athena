class CreateMenus < ActiveRecord::Migration[5.2]
  def change
    create_table :menus do |t|
      t.string :name
      t.string :link
      t.string :icon
      t.boolean :enabled, default: true

      t.timestamps
    end
  end
end
