class AddRegisterToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :git_url, :text
    add_column :users, :linkedin_url, :text
  end
end
