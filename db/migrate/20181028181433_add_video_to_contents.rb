class AddVideoToContents < ActiveRecord::Migration[5.2]
  def change
    add_column :contents, :video, :text
  end
end
