class CreateContents < ActiveRecord::Migration[5.2]
  def change
    create_table :contents do |t|
      t.string :title
      t.string :description
      t.text :content
      t.text :url
      t.boolean :enabled, default: true
      t.integer :created_by
      t.integer :updated_by

      t.timestamps
    end
  end
end
