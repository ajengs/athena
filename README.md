# Athena

Athena is a web based information system designed to support GO-Academy program from GO-JEK.

## Installation
1. Clone repository

2. Make sure docker is installed
- Install [docker](https://docs.docker.com/install/)
- Install [docker-compose](https://docs.docker.com/compose/install/)

3. Setup docker DB & generate config data
```bash
docker-compose run athena rake db:create db:migrate db:config_migration[youradminemailhere@gmail.com]
```

4. Run server
```
docker-compose up
```
