module ContentsHelper
  MODULES = {
    'Core Engineering' => 'Core Engineering',
    'DevOps' => 'DevOps'
  }

  ACCESSES = {
    'public' => 'public',
    'internal' => 'internal'
  }

  BATCHES = (1..5).inject({}) do |batches, i|
    batches["Batch #{i}"] = i
    batches
  end
end
