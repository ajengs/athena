class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  helper_method :current_user, :can_administer?

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  def can_administer?
    current_user.is_admin?
  end

  def check_session
    if current_user.nil?
      redirect_to main_app.errors_403_path
    end
  end

  def check_if_admin
    unless current_user.is_admin?
      redirect_to errors_403_path
    end
  end

  def authorized_to_modify
    unless current_user.is_authorized_to_modify?
      redirect_to errors_403_path
    end
  end

  def check_if_participant
    if !current_user.is_participant? && !current_user.is_public_participant?
      redirect_to errors_403_path
    end
  end
end
