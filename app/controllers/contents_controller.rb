class ContentsController < ApplicationController
  before_action :check_session
  before_action :authorize, except: [:index, :show]
  before_action :set_content, only: [:show, :edit, :update, :destroy, :approve]

  def index
    @contents = scoped_contents.where(enabled: true)
    @contents_in_review = Content.tagged_with('material', on: :type)
      .where(enabled: false)

    if params[:term]
      @contents = @contents
        .where("title LIKE :term OR description LIKE :term", term: "%#{params[:term]}%")
      @contents_in_review = @contents_in_review
        .where("title LIKE :term OR description LIKE :term", term: "%#{params[:term]}%")
    elsif params[:tag_query]
      @contents = @contents
        .tagged_with(params[:tag_query], on: :tags, any: true)
      @contents_in_review = @contents_in_review
        .tagged_with(params[:tag_query], on: :tags, any: true)
    end

    @contents = @contents
      .page(params[:page]).per(10)
    @contents_in_review = @contents_in_review
      .page(params[:page]).per(10)
  end

  def show
    begin
      @contents = scoped_contents.tagged_with(@content.tag_list, on: :tags, any: true).where.not(id: @content.id).limit(10)
    rescue ActiveRecord::RecordNotFound => e
      redirect_to errors_404_path
    end
  end

  def new
    @content = Content.new
  end

  def edit
  end

  def create
    @content = Content.new(content_params)
    @content.created_by = current_user.id
    @content.enabled = current_user.role.name == 'admin'

    if @content.save
      redirect('Content was successfully created.')
    else
      render :new
    end
  end

  def update
    @content.updated_by = current_user.id

    if @content.update(content_params)
      redirect('Content was successfully updated.')
    else
      render :edit
    end
  end

  def approve
    if current_user.is_admin?
      @content.update(enabled: true)
      redirect_to contents_path
    else
      redirect_to errors_403_path
    end
  end

  def authorize
    authorized_to_modify
  end

  def scoped_contents
    if current_user.is_public_participant?
      Content.tagged_with('material', on: :type).tagged_with('public', on: :access).where(enabled: true)
    elsif current_user.is_authorized_to_modify?
      Content.tagged_with('material', on: :type).all
    else
      Content.tagged_with('material', on: :type).where(enabled: true)
    end
  end

  def redirect(notice)
    redirect_to @content, notice: notice
  end

  private
    def set_content
      begin
        @content = scoped_contents.find(params[:id])
      rescue ActiveRecord::RecordNotFound => e
        redirect_to errors_404_path
      end
    end

    def content_params
      params.require(:content).permit(:title, :description, :content, :url,
        :type_list, :module_list, :level_list, :tag_list, :access_list,
        :video, :attachment)
    end
end
