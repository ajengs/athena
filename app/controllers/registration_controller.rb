class RegistrationController < ApplicationController
  def new
    @user = User.new
    render :layout => false
  end

  def create
    @user = User.new(user_params)
    @user.role = Role.find_by(name:'public_participant')

    respond_to do |format|
      if @user.save
        flash[:success] = 'You have been successfully registered. Sign in to access our awesome content!'
        format.html { redirect_to root_path }
      else
        format.html { render :new, layout: false }
      end
    end
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:name, :role_id, :email, :phone, :git_url, :image, :linkedin_url, :photo)
    end
end
