class AssignmentsController < ContentsController
  def index
    @contents = scoped_contents

    if params[:term]
      @contents = @contents
        .where("title LIKE :term OR description LIKE :term", term: "%#{params[:term]}%")
    elsif params[:tag_query]
      @contents = @contents
        .tagged_with(params[:tag_query], on: :tags, any: true)
    end

    @contents = @contents
      .page(params[:page]).per(10)
  end

  def authorize
    check_if_participant
  end

  def scoped_contents
    contents = Content.tagged_with('assignment', on: :type).all
    if current_user.is_public_participant?
      contents = contents.where(created_by: current_user.id)
    elsif current_user.is_participant?
      contents = contents.joins('LEFT JOIN users ON users.id = contents.created_by').where('users.batch = ?', current_user.batch)
    end
    contents
  end

  def redirect(notice)
    redirect_to assignments_path, notice: 'Assignment submitted'
  end
end
