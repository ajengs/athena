class DashboardController < ApplicationController
  before_action :check_session
  def index
    puts "current_user.role #{current_user.role.name}"
    if current_user && current_user.role.nil?
      redirect_to errors_403_path
    end

    @survey = Rapidfire::Survey.find_by(name: 'Happy Sheet')
    @questions = @survey.questions.where(type: 'Rapidfire::Questions::Radio')

    data = @questions.map do |q|
      sum = q.answers.inject(0.0) { |sum, a| sum + a.answer_text.to_f }
      { label: q.question_text, value: sum/q.answers.size }
    end

    @chart = Fusioncharts::Chart.new({
        width: "100%",
        height: "400",
        type: "bar2d",
        renderAt: "chartContainer",
        dataSource: {
            chart: {
              caption: "Average Happy Metrics Last 2 Weeks",
              subCaption: "t",
              xAxisname: "Question",
              yAxisName: "Happiness",
              numberPrefix: "",
              theme: "gammel",
              exportEnabled: "1",
              aligncaptionwithcanvas: "0",
              plottooltext: "<b>$dataValue</b> rating",
            },
            data: data
        }
    })

    @text_questions = @survey.questions.where('type in (?, ?)', 'Rapidfire::Questions::Long', 'Rapidfire::Questions::Short')
    @recent_feedbacks = @text_questions.map do |qt|
      qt.answers.map do |at|
        {
          question: qt.question_text,
          answer: at.answer_text
        }
      end
    end
    @recent_feedbacks = @recent_feedbacks.flatten.sort { |x,y| y[:answer].length <=> x[:answer].length }.first(5)
  end
end
