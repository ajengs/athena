class Content < ApplicationRecord
  default_scope { order(created_at: :desc) }
  acts_as_taggable # Alias for acts_as_taggable_on :tags
  acts_as_taggable_on :type, :module, :level, :access
  mount_uploader :video, VideoUploader
  mount_uploader :attachment, FileUploader

  validates_processing_of :video
  before_save :append_tag_list

  private
  def append_tag_list
    tag_list << module_list unless tag_list.include? module_list
    tag_list << level_list unless tag_list.include? level_list
  end
end
