class User < ApplicationRecord
  default_scope { order(created_at: :desc) }
  mount_uploader :photo, ImageUploader

  FORMAT_URL = /\A(https?):\/\/([\w\d]+([\-\.]{1}[\w\d]+)*(\.\w{2,})|(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))(:\d{1,5})?((\/|\?).*)?\z/ix
  GMAIL_DOMAINS = %w{gmail.com googlemail.com}

  belongs_to :role
  validates :email, presence: true, format: { with: /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i }
  validates :name, presence: true
  validates :email, uniqueness: true, if: :never_signed_in
  validates :phone, presence: true, unless: :uid
  validates :phone, uniqueness: true, allow_blank: true
  validates :git_url, :linkedin_url, format: { with: FORMAT_URL }, allow_blank: true
  validate :confirm_gmail, if: :never_signed_in
  validates :photo, file_size: { less_than: 0.5.megabyte }
  validates_processing_of :photo


  def self.from_omniauth(auth)
    where("email = ? OR (provider = ? AND uid = ?)", auth.info.email, auth.provider, auth.uid).first_or_initialize.tap do |user|
      user.provider = auth.provider
      user.uid = auth.uid
      user.name = auth.info.name
      user.oauth_token = auth.credentials.token
      user.email = auth.info.email
      user.image = auth.info.image
      user.oauth_expires_at = Time.at(auth.credentials.expires_at)
      user.save!
    end
  end

  def self.get_name(id)
    User.find(id).name
  end

  def is_authorized_to_modify?
    is_admin? || role.name == 'mentor'
  end

  def is_admin?
    role.name == 'admin'
  end

  def is_participant?
    role.name == 'participant'
  end

  def is_public_participant?
    role.name == 'public_participant'
  end

  private
  def confirm_gmail
    unless email_is_gmail
      errors[:email] << 'can only be Gmail for better integration'
    end
  end

  def email_is_gmail
    GMAIL_DOMAINS.each do |domain|
      return true if email.end_with?('@' + domain)
    end
    return false
  end

  def never_signed_in
    User.find_by(email: email).nil?
  end

  def photo_size_validation
    errors[:photo] << "should be less than 500KB" if photo.size > 0.5.megabytes
  end

end
