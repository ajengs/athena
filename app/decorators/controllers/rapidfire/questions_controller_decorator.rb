Rapidfire::QuestionsController.class_eval do
  def index
    @questions = @survey.questions
    @questions = @questions.page(params[:page])
  end

  def save_and_redirect(params, on_error_key)
    @question_form = Rapidfire::QuestionForm.new(params)
    @question_form.save

    if @question_form.errors.empty?
      respond_to do |format|
        format.html { redirect_to on_error_key == :edit ? index_location : new_question_location }
        format.js
      end
    else
      respond_to do |format|
        format.html { render on_error_key.to_sym }
        format.js
      end
    end
  end

  def index_location
    rapidfire.survey_questions_url(@survey)
  end

  def new_question_location
    rapidfire.new_survey_question_url(@survey)
  end
end