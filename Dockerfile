FROM ruby:2.3.4
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev

WORKDIR /athena

COPY Gemfile Gemfile
COPY Gemfile.lock Gemfile.lock
RUN bundle install

COPY . .

EXPOSE 3000
CMD ["rails", "s"]
