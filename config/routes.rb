Rails.application.routes.draw do
  resources :contents do
    member do
      get 'approve'
    end
  end
  # get 'homepage/index'
  root 'homepage#index', as: 'root'

  get 'auth/:provider/callback', to: 'sessions#create'
  get 'auth/failure', to: redirect('/')
  get 'signout', to: 'sessions#destroy', as: 'signout'

  resources :sessions, only: [:create, :destroy]
  resources :dashboard, only: [:index]
  resources :users

  get 'errors/403', to: 'errors#error_403'
  get 'errors/404', to: 'errors#error_404'

  mount Rapidfire::Engine => "/rapidfire"
  resources :assignments
  resources :registration, only: [:new, :create]
end
