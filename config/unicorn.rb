if !ENV['LOG_TO_STDOUT']
  stderr_path "log/unicorn_err.log"
  stdout_path "log/unicorn.log"
end

if !ENV['UNICORN_LISTEN_TO_TCP']
  listen "/var/run/saudagar/saudagar.sock"
end

if ENV['WEB_WORKER_COUNT']
  worker_processes ENV['WEB_WORKER_COUNT'].to_i
else
  worker_processes 2
end
