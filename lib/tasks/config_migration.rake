namespace :db do
  desc "Simulate db:migration (one-by-one, up then down) while recording raw SQL query. Must use RAILS_ENV=test arg"
  task :config_migration, [:admin_email]  do
    connection = ActiveRecord::Base.connection
    admin_email = args[:admin_email]

    STATEMENTS.each do |sql|
      connection.execute(sql)
    end
    connection.execute("INSERT INTO `users`
      (`id`, `email`, `created_at`, `updated_at`, `role_id`) VALUES
      (1, #{admin_email},#{Time.now},#{Time.now},1)")
  end
end

STATEMENTS = [
"INSERT INTO `roles` (`id`, `name`, `description`, `enabled`, `created_at`, `updated_at`)
VALUES
  (1,'admin',NULL,NULL,'2018-10-27 07:39:49','2018-10-27 07:39:49'),
  (2,'mentor',NULL,NULL,'2018-10-27 07:43:26','2018-10-27 07:43:26'),
  (3,'participant',NULL,NULL,'2018-10-27 07:43:30','2018-10-27 07:43:30'),
  (4,'public_participant',NULL,NULL,'2018-10-28 13:39:57','2018-10-28 13:39:57');",

"INSERT INTO `menus` (`id`, `name`, `link`, `icon`, `enabled`, `created_at`, `updated_at`, `role_id`)
VALUES
  (1,'Home','/dashboard','home',1,'2018-10-27 10:00:19','2018-10-27 10:00:19',1),
  (2,'Users Management','/users','user',1,'2018-10-27 10:00:19','2018-10-27 10:00:19',1),
  (3,'Home','/dashboard','home',1,'2018-10-27 10:00:19','2018-10-27 10:00:19',2),
  (4,'Home','/dashboard','home',1,'2018-10-27 10:00:19','2018-10-27 10:00:19',3),
  (5,'Contents','/contents','book',1,'2018-10-27 11:56:42','2018-10-27 11:56:42',1),
  (6,'Contents','/contents','book',1,'2018-10-27 11:56:42','2018-10-27 11:56:42',2),
  (7,'Contents','/contents','book',1,'2018-10-27 11:56:42','2018-10-27 11:56:42',3),
  (8,'Feedback Management','/rapidfire/surveys','refresh',1,'2018-10-27 18:46:11','2018-10-27 18:46:11',1),
  (9,'Feedbacks','/rapidfire/surveys','refresh',1,'2018-10-27 18:46:11','2018-10-27 18:46:11',3),
  (10,'Feedbacks','/rapidfire/surveys','refresh',1,'2018-10-27 18:46:11','2018-10-27 18:46:11',2),
  (11,'Assignments','/assignments','pencil',1,'2018-10-27 18:46:11','2018-10-27 18:46:11',3),
  (12,'Assignments','/assignments','pencil',1,'2018-10-27 18:46:11','2018-10-27 18:46:11',1),
  (13,'Assignments','/assignments','pencil',1,'2018-10-27 18:46:11','2018-10-27 18:46:11',2),
  (14,'Home','/dashboard','home',1,'2018-10-27 10:00:19','2018-10-27 10:00:19',4),
  (15,'Contents','/contents','book',1,'2018-10-27 11:56:42','2018-10-27 11:56:42',4),
  (16,'Assignments','/assignments','pencil',1,'2018-10-27 18:46:11','2018-10-27 18:46:11',4),
  (17,'Feedbacks','/rapidfire/surveys','refresh',1,'2018-10-27 18:46:11','2018-10-27 18:46:11',4);"
]